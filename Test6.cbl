       IDENTIFICATION DIVISION.
       PROGRAM-ID. MOVESTRING.
       AUTHOR. VIRAPAT.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  SURNAME            PIC X(8) VALUE 'COUGHLAN'.
       01  MONEY              PIC 9(4)V99.
       
       PROCEDURE DIVISION.
       BEGINPROGRAM.
           DISPLAY SURNAME
           MOVE 'EARTH' TO SURNAME
           DISPLAY 'MOVE EARTH TO SURNAME : ' SURNAME
           MOVE 'YOUNGEARTH' TO SURNAME
           DISPLAY 'MOVE YOUNGEARTH TO SURNAME : '
           DISPLAY SURNAME

           DISPLAY '---------------------------------------------------'

           DISPLAY MONEY
           MOVE 25.5 TO MONEY
           DISPLAY MONEY
           MOVE 1975 TO MONEY
           DISPLAY MONEY
           MOVE 12345.755 TO MONEY
           DISPLAY MONEY
           STOP RUN
           .