       IDENTIFICATION DIVISION.
       PROGRAM-ID. TEST-CONDITION2.
       AUTHOR. VIRPAT.
       
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  AGE         PIC 99      VALUE ZERO.
           88 INFANT               VALUE 0 THRU 3.
           88 YOUNGCHILD           VALUE 4 THRU 7.
           88 CHILD                VALUE 8 THRU 12.
           88 VISITOR              VALUE 13 THRU 64.
           88 PENSIONER            VALUE 65 THRU 99.
       
       01  HEIGHT-PEOPLE       PIC 999    VALUE ZERO.
       
       01  ADMISSION    PIC $99.99.
       
       PROCEDURE DIVISION.
       BEGINPROGRAM.
           PERFORM GET-INPUT
           PERFORM CALCULATE-ADMISSION
           PERFORM SHOW-ADMISSION
           STOP RUN.
       
       GET-INPUT.
           DISPLAY 'Enter age : ' WITH NO ADVANCING
           ACCEPT AGE
           DISPLAY 'Enter Height : ' WITH NO ADVANCING
           ACCEPT HEIGHT-PEOPLE.
       
       CALCULATE-ADMISSION.
           EVALUATE TRUE ALSO TRUE
               WHEN INFANT ALSO ANY MOVE 0 TO ADMISSION
               WHEN YOUNGCHILD ALSO ANY MOVE 10 TO ADMISSION
               WHEN CHILD ALSO HEIGHT-PEOPLE >= 48
                   IF HEIGHT-PEOPLE >= 48 MOVE 15 TO ADMISSION
                   ELSE MOVE 10 TO ADMISSION
               WHEN VISITOR ALSO HEIGHT-PEOPLE >= 48
                   IF HEIGHT-PEOPLE >= 48 MOVE 25 TO ADMISSION
                   ELSE MOVE 25 TO ADMISSION
               WHEN PENSIONER ALSO ANY  MOVE 10 TO ADMISSION
           END-EVALUATE.
       
       SHOW-ADMISSION.
           DISPLAY 'THE ADMISSION IS : ' ADMISSION.
       