       IDENTIFICATION DIVISION.
       PROGRAM-ID. CALCULATOR.
       AUTHOR.    VIRAPAT.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  FIRST-NUM          PIC 999.
       01  SECOND-NUM         PIC 999.
       01  RESULT             PIC 9999.

       PROCEDURE DIVISION.
       BEGINPROGRAM.
           DISPLAY 'ENTER 2 NUMBER NOT-OVER 999 TO CALCULATE :'
           DISPLAY 'FIRSTNUM : '
           ACCEPT FIRST-NUM  
           DISPLAY 'SECONDNUM  : ' 
           ACCEPT SECOND-NUM
           COMPUTE RESULT = FIRST-NUM + SECOND-NUM
           DISPLAY 'The result is : ' RESULT
           STOP RUN
           .
 
