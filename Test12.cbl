       IDENTIFICATION DIVISION.
       PROGRAM-ID. TEST-CONDITION2.
       AUTHOR. VIRPAT.
       
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  NUM1     PIC 9(5)V9(3).
       01  NUM2     PIC 9(5)V9(3).
           88 NUM2-IS-ZERO         VALUE ZERO.
       01  RESULT   PIC 9(5)V9(3).

       PROCEDURE DIVISION.
       BEGINPROGRAM.
           PERFORM 001-GET-INPUT THRU 001-EXIT
           STOP RUN
           .

       001-GET-INPUT.
           DISPLAY 'ENTER NUM1 : ' WITH NO ADVANCING
           ACCEPT NUM1
           DISPLAY 'ENTER NUM2 : ' WITH NO ADVANCING
           ACCEPT NUM2
           .

       001-COMPUTE.
           IF  NUM2-IS-ZERO THEN
               DISPLAY 'NUM2 CAN NOT BE ZERO'
               STOP RUN
           ELSE    
              COMPUTE RESULT = NUM1/NUM2
              DISPLAY 'RESULT IS : ' RESULT
           END-IF   
           .

       001-EXIT.
           EXIT
           .                  