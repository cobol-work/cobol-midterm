       IDENTIFICATION DIVISION.
       PROGRAM-ID.   VALID-CHARACTER.
       AUTHOR.       VIRAPAT.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  CHARIN        PIC X.
           88  VOWEL            VALUE "A" , "E" , "I" , "O" , "U".
           88  CONSONANT        VALUE "B" , "C" , "D" , "F" , "G" , "H"
               "J" THRU "N" , "P" THRU "T" , "V" THRU "Z".
           88  DIGITS           VALUE "0" THRU "9".
           88  VALID-CHARACTER  VALUE "A" THRU "Z" , "0" THRU "9".

       PROCEDURE DIVISION.
       BEGINPROGRAM.
           DISPLAY 'Enter character / number : '
           ACCEPT CHARIN

           PERFORM UNTIL NOT VALID-CHARACTER
                    IF  VOWEL
                        DISPLAY 'This is a vowel'
                    END-IF
                    IF CONSONANT
                        DISPLAY 'This is a consonant'
                    END-IF    
                    IF DIGITS
                        DISPLAY 'This is a digit'
                    END-IF
           MOVE SPACE TO CHARIN         
           DISPLAY 'Enter character / number : '
           ACCEPT CHARIN
           END-PERFORM

           STOP RUN.
