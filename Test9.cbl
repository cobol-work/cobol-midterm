       IDENTIFICATION DIVISION.
       PROGRAM-ID. TEST-CONDITION.
       AUTHOR.     VIRPAT.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  VALIDATION-RETURN-CODE       PIC 9 VALUE 9.
           88  DATE-IS-OK                VALUE 0.
           88  DATE-IS-INVALID           VALUE 1 THRU 8.
           88  VALID-CODE-SUPPLIED       VALUE 0 THRU 8.
      
       01  DATE-ERROR-MESSAGE          PIC X(35) VALUE SPACES.         
           88 CONDITION1     VALUE 'CONDITION1'.
           88 CONDITION2     VALUE 'CONDITION2'.
           88 CONDITION3     VALUE 'CONDITION3'.  
           88 CONDITION4     VALUE 'CONDITION4'.  
           88 CONDITION5     VALUE 'CONDITION5'.  
           88 CONDITION6     VALUE 'CONDITION6'.  
           88 CONDITION7     VALUE 'CONDITION7'.  
           88 CONDITION8     VALUE 'CONDITION8'.
           
       PROCEDURE DIVISION.
       BEGINPROGRAM.
           PERFORM VALIDATION-DATE UNTIL VALID-CODE-SUPPLIED
      *       GET VALUE TO MAKE A CONDITION
              EVALUATE VALIDATION-RETURN-CODE
      *       SET DATE-ERROR-MESSAGE BY VALUE OFVALIDATION-RETURN-CODE    
                 WHEN 0 SET DATE-IS-OK TO TRUE
                 WHEN 1 SET CONDITION1 TO TRUE
                 WHEN 2 SET CONDITION2 TO TRUE
                 WHEN 3 SET CONDITION3 TO TRUE
                 WHEN 4 SET CONDITION4 TO TRUE
                 WHEN 5 SET CONDITION5 TO TRUE
                 WHEN 6 SET CONDITION6 TO TRUE
                 WHEN 7 SET CONDITION7 TO TRUE
                 WHEN 8 SET CONDITION8 TO TRUE
              END-EVALUATE

              IF DATE-IS-OK THEN
                 DISPLAY 'DATE-IS-OK'
              END-IF

              IF DATE-IS-INVALID THEN
                 DISPLAY DATE-ERROR-MESSAGE
              END-IF 

              STOP RUN  
           .          

       VALIDATION-DATE.
           DISPLAY "Enter a validation return code :" WITH NO ADVANCING
           ACCEPT VALIDATION-RETURN-CODE
           .    