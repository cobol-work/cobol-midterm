       IDENTIFICATION DIVISION.
       PROGRAM-ID.    FOOD-SALE.
       AUTHOR.        VIRAPAT.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT 100-INPUT-FILE ASSIGN TO 'FOODSALE.DAT'
           ORGANIZATION IS SEQUENTIAL
           FILE STATUS IS WS-INPUT-FILE-STATUS.

           SELECT 200-OUTPUT-FILE ASSIGN TO 'FOODSALE-REPROT.RPT'
           ORGANIZATION IS LINE SEQUENTIAL
           FILE STATUS IS WS-OUTPUT-FILE-STATUS.

       DATA DIVISION.
       FILE SECTION.
       FD  100-INPUT-FILE BLOCK CONTAINS 0 RECORDS.
       01  100-INPUT-RECORD.
           05 BRANCH-ID      PIC 9(1).
           05 FILLER         PIC X(7).
           05 FOOD-ID        PIC X(4).
           05 FILLER         PIC X(8).
           05 FOOD-NAME      PIC X(23).
           05 FOOD-PRICE     PIC 9(4).
           05 FILLER         PIC X(5).
           05 FOOD-QTY       PIC 9(1).
           05 FILLER         PIC X(7).
           05 DAY-SALE       PIC X(10).
           05 FILLER         PIC X(1).
           05 TIME-SALE      PIC X(5).
           05 FILLER         PIC X(4).

       FD  200-OUTPUT-FILE BLOCK CONTAINS 0 RECORDS.

       01  200-OUTPUT-RECORD PIC X(80).

       WORKING-STORAGE SECTION.
       01  WS-INPUT-FILE-STATUS     PIC  X(2).
           88 FILE-OK               VALUE "00".
           88 FILE-AT-END           VALUE "10".

       01  WS-OUTPUT-FILE-STATUS     PIC  X(2).
           88 FILE-OK               VALUE "00".
           88 FILE-AT-END           VALUE "10".

       01  WS-SALE-REPORT.
           05 WS-TOTAL-SALE         PIC 9(7) VALUE ZERO.
           05 WS-TOTAL-SALE-A       PIC 9(7) VALUE ZERO.
           05 WS-TOTAL-SALE-B       PIC 9(7) VALUE ZERO.
           05 WS-TOTAL-SALE-C       PIC 9(7) VALUE ZERO.
       
       01  WS-REPORT-HEADER        PIC X(80) VALUE
           'DATE       TIME  CODE        NAME                  PRICE  
      -    'UNIT'. 
      
       01  WS-REPORT-TOTAL-HEADER  PIC X(80)
           VALUE 'BRANCH                  TOTAL'. 

       01  WS-REPORT-SALE-LINE.
           05 WS-SALE-DATE.
              10 WS-SALE-YEAR       PIC 9(4).
              10 FILLER             PIC X(1) VALUE '-'.  
              10 WS-SALE-MONTH      PIC 9(2).
              10 FILLER             PIC X(1) VALUE '-'. 
              10 WS-SALE-DAY        PIC 9(2).
           05 FILLER                PIC X(1) VALUE SPACE.    
           05 WS-SALE-TIME          PIC X(5).
           05 FILLER                PIC X(1) VALUE SPACE.
           05 WS-FOOD-ID            PIC X(4).
           05 FILLER                PIC X(8) VALUE SPACE.
           05 WS-FOOD-NAME          PIC X(23).
           05 WS-FOOD-PRICE         PIC 9(4).
           05 FILLER                PIC X(5) VALUE SPACE.
           05 WS-FOOD-QTY           PIC 9(1).
                     
       01  WS-TOTAL-SALE-LINE.
           05 TOTAL-BRANCH-ID          PIC X(5).
           05 FILLER                   PIC X(23).
           05 TOTAL-BRANCH-SALE        PIC 9(7).
           
       PROCEDURE DIVISION.
       0000-MAIN-PROGRAM.
           PERFORM 1000-INITIAL THRU 1000-EXIT
           PERFORM 2000-PROCESS THRU 2000-EXIT
              UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS
           PERFORM 3000-END     THRU 3000-EXIT
           STOP RUN
           .
       1000-INITIAL.   
           PERFORM 1100-OPEN-FILE-OUTPUT THRU 1100-EXIT
           PERFORM 1200-OPEN-FILE-INPUT THRU 1200-EXIT
           MOVE WS-REPORT-HEADER TO 200-OUTPUT-RECORD
           PERFORM 7000-WRITE THRU 7000-EXIT
           .
       1000-EXIT.
           EXIT
           .
       1100-OPEN-FILE-OUTPUT.
           OPEN OUTPUT 200-OUTPUT-FILE
           IF NOT FILE-OK OF WS-OUTPUT-FILE-STATUS
              DISPLAY 'FILE NOT FOUND'
           END-IF
           .

       1100-EXIT.
           EXIT
           .

       1200-OPEN-FILE-INPUT.
           OPEN INPUT 100-INPUT-FILE
           IF NOT FILE-OK OF WS-INPUT-FILE-STATUS
              DISPLAY 'FILE NOT FOUND'
           END-IF
           PERFORM 8000-READ THRU 8000-EXIT
           .

       1200-EXIT.
           EXIT
           .

       2000-PROCESS.
           DISPLAY 100-INPUT-RECORD
           PERFORM 2200-MOVE-TO-REPORT-SALE-LINE THRU 2200-EXIT
           PERFORM 7000-WRITE THRU 7000-EXIT.    
           PERFORM 2100-TOTAL-SALE THRU 2100-EXIT
           PERFORM 8000-READ THRU 8000-EXIT
           .

       2000-EXIT.
           EXIT
           .

       2100-TOTAL-SALE.
           COMPUTE WS-TOTAL-SALE =WS-TOTAL-SALE +(FOOD-PRICE * FOOD-QTY)
           IF BRANCH-ID = "A" THEN
              COMPUTE WS-TOTAL-SALE-A =
              WS-TOTAL-SALE-A +(FOOD-PRICE * FOOD-QTY)
           ELSE IF BRANCH-ID = "B" THEN
              COMPUTE WS-TOTAL-SALE-B =
              WS-TOTAL-SALE-B +(FOOD-PRICE * FOOD-QTY)
           ELSE
              COMPUTE WS-TOTAL-SALE-C =
              WS-TOTAL-SALE-C +(FOOD-PRICE * FOOD-QTY)
           END-IF        
           .

       2100-EXIT.
           EXIT
           .

       2200-MOVE-TO-REPORT-SALE-LINE.
           MOVE DAY-SALE TO WS-SALE-DATE.
           MOVE TIME-SALE TO WS-SALE-TIME
           MOVE FOOD-ID TO WS-FOOD-ID
           MOVE FOOD-NAME TO WS-FOOD-NAME
           MOVE FOOD-PRICE TO WS-FOOD-PRICE
           MOVE FOOD-QTY TO WS-FOOD-QTY
           MOVE WS-REPORT-SALE-LINE TO 200-OUTPUT-RECORD
           .

       2200-EXIT.

           .        

       3000-END.
            MOVE SPACE TO 200-OUTPUT-RECORD 
            PERFORM 7000-WRITE THRU 7000-EXIT
            
            PERFORM 3100-MOVE-REPORT-HEADER THRU 3100-EXIT
            PERFORM 7000-WRITE THRU 7000-EXIT

            MOVE 'A' TO TOTAL-BRANCH-ID
            MOVE WS-TOTAL-SALE-A TO TOTAL-BRANCH-SALE
             MOVE WS-TOTAL-SALE-LINE TO 200-OUTPUT-RECORD 
            PERFORM 7000-WRITE THRU 7000-EXIT
            DISPLAY 'TOTAL SALES A : ' WS-TOTAL-SALE-A

           
            MOVE 'B' TO TOTAL-BRANCH-ID
            MOVE WS-TOTAL-SALE-B TO TOTAL-BRANCH-SALE
            MOVE WS-TOTAL-SALE-LINE TO 200-OUTPUT-RECORD 
            PERFORM 7000-WRITE THRU 7000-EXIT
            DISPLAY 'TOTAL SALES B : ' WS-TOTAL-SALE-B
            

            MOVE 'C' TO TOTAL-BRANCH-ID
            MOVE WS-TOTAL-SALE-C TO TOTAL-BRANCH-SALE
            MOVE WS-TOTAL-SALE-LINE TO 200-OUTPUT-RECORD 
            PERFORM 7000-WRITE THRU 7000-EXIT
            DISPLAY 'TOTAL SALES C : ' WS-TOTAL-SALE-C

            MOVE 'TOTAL' TO TOTAL-BRANCH-ID
            MOVE WS-TOTAL-SALE TO TOTAL-BRANCH-SALE
            MOVE WS-TOTAL-SALE-LINE TO 200-OUTPUT-RECORD
            PERFORM 7000-WRITE THRU 7000-EXIT
            DISPLAY 'TOTAL SALES : ' WS-TOTAL-SALE

            CLOSE 100-INPUT-FILE 200-OUTPUT-FILE 
           .

       3000-EXIT.
           EXIT
           .

       3100-MOVE-REPORT-HEADER.
           MOVE WS-REPORT-TOTAL-HEADER TO 200-OUTPUT-RECORD
           .
       3100-EXIT.
           EXIT
           .        

       7000-WRITE.
           WRITE 200-OUTPUT-RECORD
           .
       7000-EXIT.
           EXIT.
           .    

       8000-READ.
           READ 100-INPUT-FILE
           .
       8000-EXIT.
           EXIT
           .


